package ajax;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean(name = "exibirForm1", eager = true)
@SessionScoped
public class ExibirForm1 implements Serializable {
   private static final long serialVersionUID = 1L;
   private String nome;
   private String pagina;
   
   public String getNome() {
      return nome;
   }
   
   public void setNome(String nome) {
      this.nome = nome;
   }
   
   public String getPagina() {
		return pagina;
	}

	public void setPagina(String pagina) {
		this.pagina = pagina;
	}   
   public String getMensagem() {
   return   "Seu nome é "
   		+ "" + nome;
   }
}