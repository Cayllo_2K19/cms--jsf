package beans;
  
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
  
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
  
import org.primefaces.event.FileUploadEvent;
  
import controle.ControleImagem;
import modelo.ModeloImagem;;
  
@ManagedBean(name = "Teste")
@ViewScoped
public class Imagens {
  
     private ModeloImagem local = new ModeloImagem();
     
     public void handleFileUpload(FileUploadEvent event) {
         
     local.setImagem(event.getFile().getContents());
      
     FacesMessage message = new FacesMessage("Succesful", event.getFile().getFileName() + " is uploaded.");
        FacesContext.getCurrentInstance().addMessage(null, message);
    }
     
     public String cadastraLocal() throws SQLException {
  
    	 ControleImagem con = new ControleImagem();
  
          if (con.insertLocal(local)) {
                FacesContext.getCurrentInstance().addMessage(
                          null,
                          new FacesMessage(FacesMessage.SEVERITY_INFO, "Sucesso!",
                                     "Local cadastrado com sucesso!"));
          } else {
                FacesContext.getCurrentInstance().addMessage(
                          null,
                          new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erro!",
                                     "Erro no cadastro de local!"));
  
          }
          con.closeConnection();
  
          return "";
     }
  
     public List<String> getImages() throws SQLException, IOException {
           
    	 ControleImagem con = new ControleImagem();
          List<ModeloImagem> listaLocais = con.listaLocais();
          List<String> images = new ArrayList<String>();
           
          String path = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/temp");
           
          for (ModeloImagem local : listaLocais) {
                FileOutputStream fos = new FileOutputStream(path + "/" + local.getNome() + ".jpg");
                fos.write(local.getImagem());
                fos.close();
                images.add(local.getNome() + ".jpg");
          }
           
          return images;
     }
  
     public ModeloImagem getLocal() {
          return local;
     }
  
     public void setLocal(ModeloImagem local) {
          this.local = local;
     }
  
}