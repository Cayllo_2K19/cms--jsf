package beans;
import java.util.ArrayList;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import controle.ControleHome;
import modelo.ModeloHome;

@ManagedBean(name="homeBean")
@RequestScoped

public class Home {
	   private String titulo;
	   private String paragrafo;
	   private int id;	   
	   ArrayList<ModeloHome> lista = new ControleHome().consultarHome();
		public ArrayList<ModeloHome> getLista() {
			return lista;
		}
		public void setLista(ArrayList<ModeloHome> lista) {
			this.lista = lista;
		}
		public ModeloHome retornarItem(int id) {
			return lista.get(id);
		}
	   public boolean enviar() {
		   boolean resultado = false;
			try {
				ControleHome homec = new ControleHome();				
				ModeloHome homem = new ModeloHome();
				homem.setTitulo(titulo);
				homem.setParagrafo(paragrafo);
				resultado = homec.inserir(homem);											
			}catch(Exception e) {
				e.getMessage();
			}
     	   return resultado;
	   }
		public boolean excluir() {
			 boolean resultado = false;
				try {
			ControleHome homec = new ControleHome();				
			//int id = 5;
			resultado = homec.excluir(id);											
		}catch(Exception e) {
			e.getMessage();
		}
		
			return resultado;
		}
		public boolean editar() {
			 boolean resultado = false;
				try {
					ControleHome homec = new ControleHome();				
					ModeloHome homem = new ModeloHome();
					int id = 1;
					homem.setId(id);
					homem.setTitulo(titulo);
					homem.setParagrafo(paragrafo);
					resultado = homec.editar(homem);											
				}catch(Exception e) {
					e.getMessage();
				}
	     	   return resultado;
		}
		public String getTitulo() {
			return titulo;
		}
		public void setTitulo(String titulo) {
			this.titulo = titulo;
		}
		public String getParagrafo() {
			return paragrafo;
		}
		public void setParagrafo(String paragrafo) {
			this.paragrafo = paragrafo;
		}
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		
		
}


