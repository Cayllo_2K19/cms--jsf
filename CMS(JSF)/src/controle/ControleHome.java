package controle;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import modelo.ModeloHome;

public class ControleHome {
	
	public ArrayList<ModeloHome> consultarHome(){
		ArrayList<ModeloHome> lista = null;
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("SELECT * FROM textos;");
			ResultSet rs = ps.executeQuery();
			if(rs != null) {
				lista = new ArrayList<ModeloHome>();
				while(rs.next()) {
					ModeloHome user = new ModeloHome();
					user.setId(rs.getInt("id"));
					user.setTitulo(rs.getString("titulo"));
					user.setParagrafo(rs.getString("paragrafo"));
					lista.add(user);
				}
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println("Erro no servidor: " + e.getMessage());
		}
		return lista;
	}


	public boolean inserir(ModeloHome Home) throws SQLException{
		boolean resultado = false;	
		try {
			Connection con = new Conexao().abrirConexao();
			String sql = "INSERT INTO textos(titulo,paragrafo) VALUES(?,?);";	
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, Home.getTitulo());
			ps.setString(2, Home.getParagrafo());
			if(!ps.execute()) {
				resultado = true;		
				new Conexao().fecharConexao(con);		
			}
		}catch(SQLException e) {
			System.out.println(e.getMessage());	
		}
		return resultado;	
	}
	
	public ModeloHome consultaID(int n) throws SQLException{
		ModeloHome a = null;
			Connection conn = new Conexao().abrirConexao();
			String sql = "SELECT * FROM textos where id= ?;";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, n);
			ResultSet rs = ps.executeQuery();		
			if(rs != null) {
				a = new ModeloHome();
				while(rs.next()) {
					a.setId(rs.getInt("id"));
					a.setTitulo(rs.getString("titulo"));
					a.setParagrafo(rs.getString("paragrafo"));
				}
				new Conexao().fecharConexao(conn);
			}else {
				throw new SQLException("Erro ao Consultar.");
			}
		return a;
	}
	public boolean editar(ModeloHome home) throws SQLException{
		boolean resultado = false;
		try {
			Connection con = new Conexao().abrirConexao();			
			String sql = "UPDATE textos SET titulo= ?,paragrafo=? WHERE id=?";			
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, home.getTitulo());
			ps.setString(2, home.getParagrafo());
			ps.setInt(3, home.getId());
			if(!ps.execute()) {
			resultado = true;
			}
			new Conexao().fecharConexao(con);		
		}catch(SQLException e) {
			System.out.println(e.getMessage());	
		}
		return resultado;	
	}
	public boolean excluir(int id) throws SQLException{
		boolean resultado = false;
		try {
			Connection con = new Conexao().abrirConexao();		
			String sql = "DELETE FROM textos WHERE id=?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1,id);
			if(!ps.execute()) {
			resultado = true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println(e.getMessage());	
		}
			return resultado;	
		}
}
