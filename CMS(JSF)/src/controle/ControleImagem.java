package controle;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import modelo.ModeloImagem;
public class ControleImagem {
      
     Connection con = null;
  
     public ControleImagem() throws SQLException {
  
          try {
                Class.forName("com.mysql.jdbc.Driver");
                System.out.println("Instalou driver");
          } catch (ClassNotFoundException e) {
                
                e.printStackTrace();
          }
  
          String url = "jdbc:mysql://localhost:3306/CMS";
          String user = "root";
          String password = "";
          con = DriverManager.getConnection(url, user, password);
     }
  
     public void closeConnection() throws SQLException {
          con.close();
     }
  
     public boolean insertLocal(ModeloImagem local) {  
          try {
                PreparedStatement preparedStatement = con
                          .prepareStatement("insert into imagens(id, nome,descrisao, imagem, data_cadastro) values(?,?,?,?,?)");
                preparedStatement.setInt(1, local.getId());
                preparedStatement.setString(2, local.getNome());
                preparedStatement.setString(3, local.getDescrisao());
                preparedStatement.setBytes(4, local.getImagem());
                preparedStatement.setDate(5, new java.sql.Date(new Date().getTime()));
  
                preparedStatement.execute();
                return true;
          } catch (SQLException ex) {
                Logger lgr = Logger.getLogger(ControleImagem.class.getName());
                lgr.log(Level.SEVERE, ex.getMessage(), ex);
                return false;
  
          }
     }
  
     public List<ModeloImagem> listaLocais() {
  
          ArrayList<ModeloImagem> lista = new ArrayList<ModeloImagem>();
  
          Statement st = null;
          ResultSet rs = null;
  
          try {
                st = con.createStatement();
                String sql = "select * from imagens ";
                rs = st.executeQuery(sql);
  
                while (rs.next()) {
  
                     ModeloImagem local = new ModeloImagem();
                     local.setId(rs.getInt(1));
                     local.setNome(rs.getString(2));
                     local.setDescrisao(rs.getString(3));
                     local.setImagem(rs.getBytes(5));
                     local.setDataCadastro(rs.getDate(6));
                     lista.add(local);
                }
  
          } catch (SQLException ex) {
                Logger lgr = Logger.getLogger(ControleImagem.class.getName());
                lgr.log(Level.SEVERE, ex.getMessage(), ex);
  
          } finally {
                try {
                     if (rs != null) {
                          rs.close();
                     }
                     if (st != null) {
                          st.close();
                     }
                     if (con != null) {
                          con.close();
                     }
  
                } catch (SQLException ex) {
                     Logger lgr = Logger.getLogger(ControleImagem.class.getName());
                     lgr.log(Level.WARNING, ex.getMessage(), ex);
                }
          }
          return lista;
     }
}