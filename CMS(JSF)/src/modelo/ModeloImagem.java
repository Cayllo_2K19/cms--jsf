package modelo;

import java.util.Date;
  
public class ModeloImagem {
      
     private int id;
     private String nome;
     private String descrisao;
     private byte[] imagem;
     private Date dataCadastro;
      
     public int getId() {
          return id;
     }
     public void setId(int id) {
          this.id = id;
     }
     public String getNome() {
          return nome;
     }
     public void setNome(String nome) {
          this.nome = nome;
     }
     public String getDescrisao() {
          return descrisao;
     }
     public void setDescrisao(String descrisao) {
          this.descrisao = descrisao;
     }
     public byte[] getImagem() {
          return imagem;
     }
     public void setImagem(byte[] imagem) {
          this.imagem = imagem;
     }
     public Date getDataCadastro() {
          return dataCadastro;
     }
     public void setDataCadastro(Date dataCadastro) {
          this.dataCadastro = dataCadastro;
     }
}