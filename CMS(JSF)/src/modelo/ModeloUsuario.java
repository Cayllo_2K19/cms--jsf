package modelo;

public class ModeloUsuario {
	private  int id;
	private  String senha;
	private  String email;
	private  String iv;
	private  String keey;
	
	public String getIv() {
		return iv;
	}
	public void setIv(String iv) {
		this.iv = iv;
	}
	public String getKeey() {
		return keey;
	}
	public void setKeey(String keey) {
		this.keey = keey;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}		
	}
